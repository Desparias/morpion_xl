This project wont be translated in english, my sentences and variables are in french, you must translate ( or not ) in your language if you want to understand most of my explanations.

This project is an imagined and [python](https://python.org) coded by [Desparias](https://gitlab.com/Desparias).

<details><summary>Project stats</summary>

|            | Iterative version (1.0) | [O.O.P](https://en.wikipedia.org/wiki/Object-oriented_programming) version (2.0) |
| :--------- | ----------------------- | ----------------------- |
| length     | 86 lines in unreadable mode but 184 lines with all the non-used part of the code | 88 lines (87 + 1 line to launch the program) |
| display    | terminal | terminal |  
| clean code | beautiful.fake | object-oriented programming = good/OK |  
| size       | 7 ko | 4 ko |  
| files      | 1 | 1 |  
</details>

Any help or advise is welcome.  

This project which is a game is F2P. Both version are available and playable.  
You just need python on your computer.  
 
You can tell/ask anything you want to help/understand this project.

Thanks for reading, and maybe playing.
