class MORPION():
    def __init__(self):
        self.grille = [ [ "•" for _ in range(9) ] for _ in range(9) ]
        self.morpion_actuel = 4
        self.morpion_gagnés = [ "•" for _ in range(9) ]
        self.joueur = 'O'

        self.inverseur = {7:0, 8:1, 9:2, 4:3, 5:4, 6:5, 1:6, 2:7, 3:8}
        self.formes = [ [0,1,2], [3,4,5], [6,7,8],  [0,3,6], [1,4,7], [2,5,8], [0,4,8], [2,4,6] ]

        for _ in range(9*9):
            self.affiche()
            self.choix_joueur(self.joueur)
            self.win_check()
            if self.joueur == "O": self.joueur = "X"
            else: self.joueur = "O"
            

    def affiche(self):
        from os import system
        try: system('CLS')
        except: system('clear')

        for i in range(3):
            print(' +---------+---------+---------+ ')

            if i == 0: T = [0,3]
            elif i == 1: T = [3,6]
            elif i == 2: T = [6,9]

            for i1 in range(T[0],T[1]): # ligne des 0 1 2
                print(" | ",end=" ")
                for i2 in range(3): print(self.grille[i1][i2], end=" ") # 0 1 2
            print(' | ')

            for i1 in range(T[0],T[1]): # ligne des 3 4 5
                print(" | ",end=" ")
                for i2 in range(3,6): print(self.grille[i1][i2], end=" ") # 3 4 5
            print(' | ')
        

            for i1 in range(T[0],T[1]): # ligne des 6 7 8
                print(" | ",end=" ")
                for i2 in range(6,9): print(self.grille[i1][i2], end=" ") # 6 7 8
            print(' | ')
        print(' +---------+---------+---------+ ')

    def choix_joueur(self, joueur):
        case_choisit = str(input(f"{self.joueur}:"))
        if case_choisit == "auto_end":
            for i in range(3):
                self.morpion_gagnés[i] = "O"
            self.win_check()
        elif case_choisit == "new_game":
            self.grille = MORPION().grille
            self.affiche()
        else:
            case_choisit = self.inverseur[int(case_choisit)]

        if self.grille[self.morpion_actuel][case_choisit] != "•":
            print("La case désignée est malheureusement injouable (déjà joué ou inacessible), choisissez encore.")
            return self.choix_joueur(joueur)
        else:
            self.grille[self.morpion_actuel][case_choisit] = joueur
            if "•" in self.grille[case_choisit]:
                self.morpion_actuel = case_choisit
            else:
                print("Le prochain morpion étant plein, le joueur suivant reste sur le même morpion.")

    def win_check(self):
        from sys import exit
        
        for j_à_v in ['O', 'X']:
            for i in range(9):
                if self.morpion_gagnés[i] == '•': 
                    for a, b, c in self.formes:
                        if self.grille[i][a] == j_à_v and self.grille[i][b] == j_à_v and self.grille[i][c] == j_à_v:
                            self.morpion_gagnés[i] = j_à_v
        
        for j_à_v in ['O', 'X']: 
            for a, b, c in self.formes:
                if self.morpion_gagnés[a] == j_à_v and self.morpion_gagnés[b] == j_à_v and self.morpion_gagnés[c] == j_à_v:
                    print(f"Le Joueur {j_à_v} a gagné !")
                    exit()
                if not "•" in self.morpion_gagnés:
                    print("Match Nul !")
                    exit()
MORPION()
