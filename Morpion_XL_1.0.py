""" """
from os import system ; import sys
""" """
mpXL = [ ['•' for _ in range (9) ] for _ in range (9) ]
p_mp_g = ['•' for _ in range(9)]
formes = [[0,1,2], [3,4,5], [6,7,8],[0,3,6], [1,4,7], [2,5,8],[0,4,8], [2,4,6]]
p_mp, j_a = 4, 'O'
""" """

def affichage(mpXL: list,p_mp: int, option: int) -> str:
    if option == 1:
        """ Affiche le plateau du jeu (option 1)

        >>> affichage(mpXL,p_mp,1)
        
        '            +-------+ '
        '            | 0 1 2 | '
        '            | 3 ♦ 5 | '
        '            | 6 7 8 | '
        '            +-------+ '
        
        """
        
        print("            +-------+ ")
        print("            ",end="")
        print('| ',end="")
        for i in range(0,3):
            if i == p_mp: print('♦',end=" ")
            else: print(i+1,end=" ")
        print('| ')
        print("            ",end="")
        print('| ',end="")
        for i in range(3,6):
            if i == p_mp: print('♦',end=" ")
            else: print(i+1,end=" ")
        print('| ')
        print("            ",end="")
        print('| ',end="")
        for i in range(6,9):
            if i == p_mp: print('♦',end=" ")
            else: print(i+1,end=" ")
        print('| ')
        print("            +-------+ ")

    if option == 2:
        """ Affiche l'aide du joueur (option 2)
        
        >>> affichage(mpXL,p_mp,2)

        ' +---------+---------+---------+ '
        ' |  • • •  |  • • •  |  • • •  | '
        ' |  • • •  |  • • •  |  • • •  | '
        ' |  • • •  |  • • •  |  • • •  | '
        ' +---------+---------+---------+ '
        ' |  • • •  |  • • •  |  • • •  | '
        ' |  • • •  |  • • •  |  • • •  | '
        ' |  • • •  |  • • •  |  • • •  | '
        ' +---------+---------+---------+ '
        ' |  • • •  |  • • •  |  • • •  | '
        ' |  • • •  |  • • •  |  • • •  | '
        ' |  • • •  |  • • •  |  • • •  | '
        ' +---------+---------+---------+ '

        """

        for i in range(3):
            print(' +---------+---------+---------+ ')

            if i == 0: T = [0,3]
            elif i == 1: T = [3,6]
            elif i == 2: T = [6,9]

            for i1 in range(T[0],T[1]): # ligne des 0 1 2
                print(" | ",end=" ")
                for i2 in range(3): print(mpXL[i1][i2],end=" ") # 0 1 2
            print(' | ')

            for i1 in range(T[0],T[1]): # ligne des 3 4 5
                print(" | ",end=" ")
                for i2 in range(3,6): print(mpXL[i1][i2],end=" ") # 3 4 5
            print(' | ')
        

            for i1 in range(T[0],T[1]): # ligne des 6 7 8
                print(" | ",end=" ")
                for i2 in range(6,9): print(mpXL[i1][i2],end=" ") # 6 7 8
            print(' | ')
        print(' +---------+---------+---------+ ')

    if option == 3:
        """ Affiche un morpion représentant ceux gagnés (option 3)
            
        >>> affichage(mpXL,p_mp,3)

        '• • •'
        '• • •'
        '• • •'

        """

        for a,b,c in [[0,1,2],[3,4,5],[6,7,8]]:
            print(' ',p_mp_g[a],p_mp_g[b],p_mp_g[c])

def vérifications(mpXL: list, p_mp_g: int, affichage, option: int) -> None:
    if option == 1:
        """ Marque les petits_morpions gagnants
 
        >>> vérifications(mpXL,p_mp_g,affichage,1)
        None

        """
        for j_à_v in ['O', 'X']:
            for i in range(9):
                if p_mp_g[i] == '•': 
                    for a, b, c in formes:
                        if mpXL[i][a] == j_à_v and mpXL[i][b] == j_à_v and mpXL[i][c] == j_à_v:
                            p_mp_g[i] = j_à_v

    if option == 2:
        """ Vérifit si une joueur a gagné le mpXL (donc le grand morpion)
        
        >>> vérifications(mpXL,p_mp_g,affichage,2)
        None

        """
        for j_à_v in ['O', 'X']: 
            for a, b, c in formes:
                if p_mp_g[a] == j_à_v and p_mp_g[b] == j_à_v and p_mp_g[c] == j_à_v:
                    print(f"Le Joueur {j_à_v} a gagné !")
                    sys.exit()
                if not "•" in p_mp_g:
                    print("Match Nul !")
                    sys.exit()

def jeu_mpXL(affichage, vérifications,p_mp_g, mpXL, p_mp, j_a):
    """ Cette fonction fait fonctionner le mpXL à l'infini

    >>> jeu_mpXL(affichage, vérifications,p_mp_g, mpXL, p_mp, j_a)
    None
    """

    for i in range(1, 4): affichage(mpXL,p_mp,i)

    ### Input ###
    print(f"Quelle case désirez-vous Joueur {j_a} ?")
    c_j = int(input())-1
    if not 0 <= c_j < len(p_mp_g):
        print("Choisissez entre 0 et 8 s'il  vous plait...")
        return jeu_mpXL(affichage,vérifications,p_mp_g,mpXL,p_mp,j_a)

    # Verif Case choisit par le joueur change si pas prise, sinon redemande
    if mpXL[p_mp][c_j] == '•':
        mpXL[p_mp][c_j] = j_a
        p_mp = c_j
    else:
        print(f"Cette case est déjà prise, choisissez en une autre.)")
        return jeu_mpXL(affichage,vérifications,p_mp_g,mpXL,p_mp,j_a)

    for i in range(1, 3): vérifications(mpXL,p_mp_g,affichage,i)
    
    # Change de joueur
    if j_a == 'O': j_a = 'X'
    else: j_a = 'O'

    try: system('CLS')
    except: system('clear')

    return jeu_mpXL(affichage,vérifications,p_mp_g,mpXL,p_mp,j_a)

### Output ###
jeu_mpXL(affichage, vérifications,p_mp_g, mpXL, p_mp, j_a)
### ###### ###

Vocabulaire = {
               "moprionXL": mpXL,
               "petit_morpion_gagnant": p_mp_g,
               "paternes_de_victoire": formes,
               "petit_morpion": p_mp,
               "joueur_actuel": j_a,
               "joueur_à_vérifier": j_à_v,
               "choix_joueur": c_j,
               "fonction_d'affichage": affichage(mpXL, p_mp, option),
               "fonction_de_vérification": vérifications(mpXL, p_mp_g, affichage, option),
                }
